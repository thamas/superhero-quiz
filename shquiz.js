// Model -----------------------------------------------------------------------
// Quiz data
var quiz = {
  description: 'Superhero name quiz',
  question: 'What\'s the real name of ',
  heroes: [
    {
      name: 'Superman',
      realName: 'Clark Kent'
    },
    {
      name: 'Batman',
      realName: 'Bruce Wayne'
    },
    {
      name: 'Spiderman',
      realName: 'Peter Parker'
    }
  ]
};

// View ------------------------------------------------------------------------
// Page elements
var $message = document.getElementById('message');
var $scoreBlock = document.getElementById('score-block');
var $score = document.getElementById('score');
var $feedback = document.getElementById('feedback');
var $start = document.getElementById('start');
var $form = document.getElementById('answer');
var $answerInput = document.getElementById('answer__input');

// Display functions
function update(element, content, cssClass) {
  var el = element.firstChild || document.createElement('span');
  el.textContent = content;
  element.appendChild(el);
  if (cssClass) {
    el.className = cssClass;
  }
}

function hide(element) {
  element.style.display = 'none';
}

function show(element) {
  element.style.display = 'initial';
}

// Controller ------------------------------------------------------------------

// Display only the info and start button by default
hide($scoreBlock);
hide($form);

// Make possible to use Enter key to start
document.addEventListener('keyup', enKeyUp);
function enKeyUp(e) {
  if (e.keyCode == 13) {
    play(quiz);
    // Remove after one use to not interfere with answer submit
    document.removeEventListener('keyup', enKeyUp);
  }
}

// Game control – start
$start.addEventListener('click', function() {
  // Remove Enter event listener to not interfere with answer submit
  document.removeEventListener('keyup', enKeyUp);
  play(quiz);
}, false);

// Game control function
function play(quiz){

  var actualScore = 0;
  update($score, actualScore);

  // The common counter for nested functions
  // Must be set before selectHero call - see hoisting!
  var i = 0;

  // Change display elements s needed
  hide($start);
  hide($feedback);
  show($scoreBlock);
  show($form);

  // Start the quiz
  selectHero();

  // Select the actual hero's name
  function selectHero() {
    var heroName = quiz.heroes[i].name;
    ask(heroName);
  }

  // Display the question and clear the input
  function ask(heroName) {
    update($message, quiz.question + heroName + '?');
    $answerInput.value = '';
    $answerInput.focus();
  }

  // React to answer submit
  $form.addEventListener('submit', function(event) {
    event.preventDefault();
    var currentAnswer = $answerInput.value;
    check(currentAnswer);
    goAhead();
  }, false);

  // Check submitted answer and display related info
  function check(answer) {
    // Display the feedback section…
    show($feedback);
    // …than add content to it
    if (answer === quiz.heroes[i].realName) {
      update($feedback, 'Correct!', 'correct');
      actualScore++;
      update($score, actualScore);
    } else {
      update($feedback, 'Wrong! The real name of ' + quiz.heroes[i].name + ' is ' + quiz.heroes[i].realName + '.', 'wrong');
    }
  }

  // Go to the next question or to the end of the game
  function goAhead() {
    i++;
    if (i === quiz.heroes.length) {
      gameOver();
    } else {
      selectHero();
    }
  }

  // Game over info and possibilite to start again
  function gameOver() {
    update($message, 'Game Over! You scored ' + actualScore + ' points from the possible maximum of ' + quiz.heroes.length + '.');

    hide($scoreBlock);
    hide($form);
    show($start);
    $start.textContent = 'Play again!';

    // Make possible to use Enter key to start over again
    // It needs a timeout to not act on the last answer submit
    setTimeout(function () {
      document.addEventListener('keyup', enKeyUp);
    }, 500);
  }
}
